package com.example.planet.repositories;

import com.example.planet.entities.Crew;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CrewRepository extends JpaRepository<Crew, String> {
}
