package com.example.planet.repositories;

import com.example.planet.entities.Planet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PlanetRepository extends JpaRepository<Planet, String> {
    Optional<Planet> findByName(String name);
}
