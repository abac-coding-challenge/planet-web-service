package com.example.planet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlanetWebServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlanetWebServiceApplication.class, args);
    }
}
