package com.example.planet.controllers;

import com.example.planet.dtos.PlanetDTO;
import com.example.planet.dtos.builders.PlanetBuilder;
import com.example.planet.exceptions.AbstractException;
import com.example.planet.services.PlanetService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/planets")
public class PlanetController {
    private final PlanetService planetService;
    private final PlanetBuilder planetBuilder;

    public PlanetController(PlanetService planetService, PlanetBuilder planetBuilder) {
        this.planetService = planetService;
        this.planetBuilder = planetBuilder;
    }

    @GetMapping
    public ResponseEntity findAll() {
        return ResponseEntity.ok(planetBuilder.generateDTOsFromEntities(planetService.getAll()));
    }

    @PostMapping()
    public ResponseEntity create(@RequestBody PlanetDTO planetDTO) {
        try {
            return ResponseEntity.ok(planetBuilder.generateDTOFromEntity(planetService.add(planetDTO)));
        } catch (AbstractException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getErrorMessage());
        }
    }

    @PatchMapping(value = "/{id}")
    public ResponseEntity update(@PathVariable String id,
                                 @RequestParam(name = "crewId", required = false) String crewId,
                                 @RequestBody PlanetDTO planetDTO) {
        try {
            return ResponseEntity.ok(planetBuilder.generateDTOFromEntity(planetService.edit(id, crewId, planetDTO)));
        } catch (AbstractException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getErrorMessage());
        }
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable String id) {
        try {
            planetService.remove(id);

            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        } catch (AbstractException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getErrorMessage());
        }
    }
}
