package com.example.planet.utils.enums;

public enum Status {
    OK("OK"),
    NOT_OK("!OK"),
    TODO("TODO"),
    EN_ROUTE("En route");

    private final String value;

    Status(String value) {
        this.value = value;
    }

    public static boolean isMember(String value) {
        Status[] statuses = Status.values();

        for (Status status : statuses) {
            if (status.getValue().equals(value)) {
                return true;
            }
        }

        return false;
    }

    public String getValue() {
        return value;
    }
}
