package com.example.planet.utils.enums;

public enum ExceptionCodes {
    ALREADY_EXISTING_PLANET_NAME("ALREADY_EXISTING_PLANET_NAME"),
    CREW_NOT_FOUND_BY_ID("CREW_NOT_FOUND_BY_ID"),
    PLANET_NOT_FOUND_BY_ID("PLANET_NOT_FOUND_BY_ID"),
    PLANET_STATUS_IS_NULL_OR_IT_IS_MISTYPED("PLANET_STATUS_IS_NULL_OR_IT_IS_MISTYPED");

    private final String value;

    ExceptionCodes(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
