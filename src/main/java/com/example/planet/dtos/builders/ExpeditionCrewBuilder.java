package com.example.planet.dtos.builders;

import com.example.planet.dtos.ExpeditionCrewDTO;
import com.example.planet.entities.Crew;
import com.example.planet.entities.Robot;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ExpeditionCrewBuilder implements Builder<Crew, ExpeditionCrewDTO> {

    @Override
    public ExpeditionCrewDTO generateDTOFromEntity(Crew crew) {
        return new ExpeditionCrewDTO(
                crew.getId(),
                crew.getUser().getName(),
                crew.getRobots().stream().map(Robot::getName).collect(Collectors.toList()));
    }

    @Override
    public Crew generateEntityFromDTO(ExpeditionCrewDTO expeditionCrewDTO) {
        return new Crew(
                expeditionCrewDTO.getId(),
                null,
                null,
                null);
    }

    @Override
    public List<ExpeditionCrewDTO> generateDTOsFromEntities(List<Crew> crews) {
        List<ExpeditionCrewDTO> expeditionCrewDTOS = new ArrayList<>();

        for (Crew crew : crews) {
            expeditionCrewDTOS.add(generateDTOFromEntity(crew));
        }

        return expeditionCrewDTOS;
    }

}
