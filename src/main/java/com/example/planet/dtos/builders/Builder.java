package com.example.planet.dtos.builders;

import java.util.List;

public interface Builder<E, D> {
    D generateDTOFromEntity(E e);

    E generateEntityFromDTO(D d);

    List<D> generateDTOsFromEntities(List<E> eList);
}
