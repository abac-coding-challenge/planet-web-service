package com.example.planet.dtos.builders;

import com.example.planet.dtos.PlanetDTO;
import com.example.planet.entities.Planet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PlanetBuilder implements Builder<Planet, PlanetDTO> {

    @Autowired
    ExpeditionCrewBuilder expeditionCrewBuilder;

    @Override
    public PlanetDTO generateDTOFromEntity(Planet planet) {
        return new PlanetDTO(
                planet.getId(),
                planet.getName(),
                planet.getImage(),
                planet.getStatus(),
                planet.getDescription(),
                (planet.getCrew() == null) ? (null) : (expeditionCrewBuilder.generateDTOFromEntity(planet.getCrew())));
    }

    @Override
    public Planet generateEntityFromDTO(PlanetDTO planetDTO) {
        return new Planet(
                planetDTO.getId(),
                planetDTO.getName(),
                planetDTO.getImage(),
                planetDTO.getStatus(),
                planetDTO.getDescription(),
                null);
    }

    @Override
    public List<PlanetDTO> generateDTOsFromEntities(List<Planet> planets) {
        List<PlanetDTO> planetDTOs = new ArrayList<>();

        for (Planet planet : planets) {
            planetDTOs.add(generateDTOFromEntity(planet));
        }

        return planetDTOs;
    }

}
