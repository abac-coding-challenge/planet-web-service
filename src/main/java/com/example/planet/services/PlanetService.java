package com.example.planet.services;

import com.example.planet.dtos.PlanetDTO;
import com.example.planet.dtos.builders.PlanetBuilder;
import com.example.planet.entities.Crew;
import com.example.planet.entities.Planet;
import com.example.planet.exceptions.AbstractException;
import com.example.planet.exceptions.NotFoundException;
import com.example.planet.repositories.CrewRepository;
import com.example.planet.repositories.PlanetRepository;
import com.example.planet.utils.enums.ExceptionCodes;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.example.planet.utils.enums.Status.isMember;

@Service
public class PlanetService {
    private final PlanetRepository planetRepository;
    private final PlanetBuilder planetBuilder;
    private final CrewRepository crewRepository;

    public PlanetService(PlanetRepository planetRepository, PlanetBuilder planetBuilder, CrewRepository crewRepository) {
        this.planetRepository = planetRepository;
        this.planetBuilder = planetBuilder;
        this.crewRepository = crewRepository;
    }

    public List<Planet> getAll() {
        return planetRepository.findAll();
    }

    public Planet add(PlanetDTO planetDTO) throws AbstractException {
        Optional<Planet> optionalPlanet = planetRepository.findByName(planetDTO.getName());

        if (optionalPlanet.isPresent()) {
            throw new AbstractException(ExceptionCodes.ALREADY_EXISTING_PLANET_NAME.getValue());
        }

        if (planetDTO.getStatus() == null || !isMember(planetDTO.getStatus())) {
            throw new AbstractException(ExceptionCodes.PLANET_STATUS_IS_NULL_OR_IT_IS_MISTYPED.getValue());
        }

        Planet planet = planetBuilder.generateEntityFromDTO(planetDTO);
        planetRepository.save(planet);

        return planet;
    }

    public Planet edit(String id, String crewId, PlanetDTO planetDTO) throws AbstractException {
        Optional<Planet> optionalPlanet = planetRepository.findById(id);

        if (optionalPlanet.isEmpty()) {
            throw new NotFoundException(ExceptionCodes.PLANET_NOT_FOUND_BY_ID.getValue());
        }

        Planet planet = optionalPlanet.get();

        if (planetDTO.getName() != null) {
            optionalPlanet = planetRepository.findByName(planetDTO.getName());

            if (optionalPlanet.isPresent()) {
                throw new AbstractException(ExceptionCodes.ALREADY_EXISTING_PLANET_NAME.getValue());
            }

            planet.setName(planetDTO.getName());
        }

        if (planetDTO.getStatus() != null && isMember(planetDTO.getStatus())) {
            planet.setStatus(planetDTO.getStatus());
        }

        if (planetDTO.getImage() != null) {
            planet.setImage(planetDTO.getImage());
        }

        if (planetDTO.getDescription() != null) {
            planet.setDescription(planetDTO.getDescription());
        }

        if (crewId != null && !crewId.equals("")) {
            Optional<Crew> optionalCrew = crewRepository.findById(crewId);

            if (optionalCrew.isEmpty()) {
                throw new NotFoundException(ExceptionCodes.CREW_NOT_FOUND_BY_ID.getValue());
            }

            planet.setCrew(optionalCrew.get());
        }

        planetRepository.save(planet);

        return planet;
    }

    public void remove(String id) throws NotFoundException {
        Optional<Planet> optionalPlanet = planetRepository.findById(id);

        if (optionalPlanet.isEmpty()) {
            throw new NotFoundException(ExceptionCodes.PLANET_NOT_FOUND_BY_ID.getValue());
        }

        planetRepository.delete(optionalPlanet.get());
    }
}
