package com.example.planet.exceptions;

import com.example.planet.models.ErrorResponse;
import org.springframework.http.HttpStatus;

public class AbstractException extends Exception {
    private final HttpStatus statusCode = HttpStatus.BAD_REQUEST;

    public AbstractException(String message) {
        super(message);
    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }

    public ErrorResponse getErrorMessage() {
        return new ErrorResponse(this.getMessage());
    }
}
